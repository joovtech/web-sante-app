import { AuthChecker } from "type-graphql";
import { authToken } from "./authToken";
import { Context } from "./context";
import { User } from "@generated/type-graphql/models/User";

export const customAuthChecker: AuthChecker<Context> = async ({
  context,
}): Promise<boolean> => {
  const { token, prisma } = context;
  const user = await authToken.verify<User>(token);
  if (user) {
    const findUser = await prisma.user.findUnique({
      where: { email: user.email },
    });
    return findUser ? true : false;
  }
  return false;
};
