//@ts-nocheck
import { FileExt, Message } from "@generated/type-graphql/models";
import { Field, InputType, ObjectType } from "type-graphql";

@InputType({ description: "This input is for importing files" })
export class InputFile implements Partial<FileExt> {
  @Field()
  name: string;

  @Field()
  url: string;

  @Field()
  extension: string;
}

@InputType({ description: "This is input for sending message" })
export class MessageInput implements Partial<Message> {
  @Field()
  userId?: number;

  @Field()
  content: string;

  @Field(() => [InputFile])
  files?: InputFile[];
}
@ObjectType({ description: "return type of query message" })
export class MessageExtend extends Message {
  @Field(() => [FileExt])
  files?: FileExt[];
}
