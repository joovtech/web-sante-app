import { Arg, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Discussion, Message, FileExt } from "@generated/type-graphql/models";
import { Context } from "../../context";
import { GraphQLError } from "graphql";
import { MessageExtend, MessageInput } from "./type";

@Resolver(Discussion)
export class DiscussionResolver {
  @Query(() => [Discussion])
  async getDiscussionsUser(
    @Arg("userId") userId: number,
    @Arg("cursor", { nullable: true }) cursor: number,
    @Arg("limit", { defaultValue: 10 }) limit: number,
    @Ctx() ctx: Context
  ) {
    try {
      const filters: any = {
        where: { userId },
        orderBy: { updatedAt: "desc" },
        take: limit,
      };
      const discussions = await ctx.prisma.discussion.findMany(
        cursor ? { ...filters, cursor: { id: cursor }, skip: 1 } : filters
      );
      return discussions;
    } catch (error) {
      return new GraphQLError("une erreur s'est produite");
    }
  }

  @Query(() => [MessageExtend])
  async getMessagesOfDiscussion(
    @Arg("discussionId") discussionId: number,
    @Arg("cursor", { nullable: true }) cursor: number,
    @Arg("limit", { defaultValue: 10 }) limit: number,
    @Ctx() ctx: Context
  ) {
    try {
      const filters: any = {
        where: { discussionId },
        include: { files: true },
        take: limit,
        orderBy: {
          updatedAt: "asc",
        },
      };
      const message = (await ctx.prisma.message.findMany(
        cursor ? { ...filters, cursor: { id: cursor }, skip: 1 } : filters
      )) as (Message & {
        files: FileExt[];
      })[];
      return message;
    } catch (error) {
      return new GraphQLError("une erreur s'est produite");
    }
  }

  @Mutation(() => Discussion)
  async sendMessage(
    @Arg("messageInput") messageInput: MessageInput,
    @Ctx() ctx: Context
  ) {
    try {
      const { userId, content, files } = messageInput;
      const discussion = await ctx.prisma.discussion.create({
        data: { User: { connect: { id: userId } } },
      });
      await ctx.prisma.message.create({
        data: {
          content,
          User: { connect: { id: userId } },
          Discussion: { connect: { id: discussion.id } },
          files: {
            createMany: {
              data: files,
            },
          },
        },
      });
      // implement here part of call models chat and get the suggestions for user
      
      return discussion;
    } catch (error) {
      return new GraphQLError("une erreur s'est produite");
    }
  }
}
