import {
  Arg,
  Authorized,
  Ctx,
  Mutation,
  PubSub,
  PubSubEngine,
  Query,
  Resolver,
} from "type-graphql";
import { User } from "@generated/type-graphql/models/User";
import { Context } from "../../context";
import { GraphQLError } from "graphql";
import Bcrypt from "bcryptjs";
import { LoginResponseForm, SignupInput, UpdateUserInput } from "./type";
import { authToken } from "../../authToken";

@Resolver(User)
export class UserResolver {
  @Query(() => User)
  async getUser(@Arg("userId") userId: number, @Ctx() ctx: Context) {
    try {
      const user = await ctx.prisma.user.findUnique({ where: { id: userId } });
      return user;
    } catch (error) {
      console.log(error);
      return new GraphQLError("une erreur s'est produite");
    }
  }
  @Mutation(() => LoginResponseForm)
  async signup(@Arg("userInput") userInput: SignupInput, @Ctx() ctx: Context) {
    try {
      const { email, password } = userInput;
      const user = await ctx.prisma.user.findUnique({
        where: {
          email: email,
        },
      });
      if (user) {
        return new GraphQLError("L'email que vous avez entrer est déjà pris");
      }
      const hashpasswd = Bcrypt.hashSync(password, 10);
      const newUser = await ctx.prisma.user.create({
        data: {
          ...userInput,
          status: true,
          password: hashpasswd,
        },
      });
      const token = authToken.sign(newUser);
      const response: LoginResponseForm = {
        message: "Vous êtes inscris",
        success: true,
        data: {
          ...newUser,
          token,
        },
      };
      return response;
    } catch (error) {
      console.log(error);
      return new GraphQLError("une erreur s'est produite");
    }
  }

  @Mutation(() => LoginResponseForm)
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @PubSub() pubsub: PubSubEngine,
    @Ctx() ctx: Context
  ) {
    try {
      if (!email || !password)
        return new GraphQLError(
          !password ? "le mot de passe est requis" : "l'email est requis"
        );
      const user = await ctx.prisma.user.findUnique({
        where: {
          email: email,
        },
      });
      if (!user)
        return new GraphQLError(
          "L'email que vous avez entré ne correspond à aucun compte"
        );
      const isValid = Bcrypt.compareSync(password, user.password);
      if (!isValid)
        return new GraphQLError(
          "Le mot de passe que vous avez entré est incorrect"
        );
      const newUser = await ctx.prisma.user.update({
        where: { email },
        data: { status: true },
      });
      const token = authToken.sign(user);
      const response: LoginResponseForm = {
        message: "Vous êtes authentifié",
        success: true,
        data: {
          ...newUser,
          token,
        },
      };
      pubsub.publish("STATUS", { userLogin: newUser });
      return response;
    } catch (error) {
      console.log(error);
      return new GraphQLError(JSON.stringify(error));
    }
  }

  @Authorized()
  @Mutation(() => String)
  async updateUser(
    @Arg("userId") userId: number,
    @Arg("updateUserInput") updateUserInput: UpdateUserInput,
    @Ctx() ctx: Context
  ) {
    try {
      await ctx.prisma.user.update({
        where: {
          id: userId,
        },
        data: updateUserInput,
      });
      return "Information mis à jour";
    } catch (error) {
      return new GraphQLError("une erreur s'est produite");
    }
  }
}
