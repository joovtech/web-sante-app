//@ts-nocheck
import { User } from "@generated/type-graphql/models";
import { IsEmail } from "class-validator";
import { Field, InputType, ObjectType } from "type-graphql";
import { ResponseForm } from "../../Types/ResponseForm";
import { UserMode } from "@generated/type-graphql/enums";
@ObjectType({ description: "User with token" })
class UserWithToken extends User {
  @Field()
  token: string;
}

@InputType({ description: "user inputs" })
export class SignupInput implements Partial<User> {
  @Field()
  @IsEmail()
  email: string;

  @Field()
  username: string;


  @Field()
  password: string;
}
@ObjectType({ description: "Login response with token" })
export class LoginResponseForm extends ResponseForm<UserWithToken> {
  @Field()
  message: string;

  @Field()
  success: boolean;

  @Field(() => UserWithToken, { nullable: true })
  data?: UserWithToken;
}

@InputType({ description: "input for update user" })
export class UpdateUserInput implements Partial<User> {
  @Field({ nullable: true })
  @IsEmail()
  email?: string;

  @Field({ nullable: true })
  firstname?: string;

  @Field({ nullable: true })
  lastname?: string;

  @Field({ nullable: true })
  civilite?: string;

  @Field({ nullable: true })
  photo?: string;

  @Field({ nullable: true })
  mode?: UserMode;
}
