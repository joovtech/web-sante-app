import { NonEmptyArray } from "type-graphql";
import { UserResolver } from "./userResolver/userResolver";
import { DiscussionResolver } from "./discussionResolver";

export const resolvers = [UserResolver, DiscussionResolver] as
  | NonEmptyArray<Function>
  | NonEmptyArray<string>;
